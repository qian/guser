# 用户模块
基于https://goframe.org/display/gf 框架的通用用户模块,支持session,token,jwt

## 创建gf项目
gf-cli安装文档: https://gitee.com/johng/gf-cli

```
gf init demo
cd demo
```

## 数据库
```
CREATE TABLE `guser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(64) NOT NULL COMMENT '[账户]',
  `password` varchar(32) DEFAULT NULL COMMENT '[密码]',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '[创建时间]',
  PRIMARY KEY (`id`),
  KEY `account` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='[用户账户]';

INSERT INTO `guser` VALUES ('1', 'admin', '108418f5eea05241cfc646796e8c5119', '2021-03-19 02:44:54');
```
将表导入数据库


## 修改配置
编辑 config/config.toml,新增:
```
[guser]
    auth = "session,token,jwt"
```
目前提供3种认证方式,可根据实际需要选择启用哪种验证方式

redis配置(已配置忽略)
```
[redis]
    default = "127.0.0.1:6379,0"
```

mysql配置(已配置忽略)

```
[database]
    link  = "mysql:root:root@tcp(127.0.0.1:3306)/demo"
```

## 项目中使用
### 1. 引入guser模块
在boot/boot.go文件中引入guser模块
```
import (
	_ "gitee.com/qian/guser"
)
```

### 2. 测试
启动项目
```
gf run main.go
```

如果项目增加以下路由即成功

| METHOD |        ROUTE         |                           HANDLER                           |                                            MIDDLEWARE                                             
|--------|----------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
|---------|---------|---------|--------|----------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| POST   | /guser/jwtLogin      | github.com/gogf/gf-jwt.(*GfJWTMiddleware).LoginHandler-fm   | guser.MiddlewareCORS,jwt.(*middlewareService).Ctx-fm                                              
|---------|---------|---------|--------|----------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| GET    | /guser/jwtLogout     | github.com/gogf/gf-jwt.(*GfJWTMiddleware).LogoutHandler-fm  | guser.MiddlewareCORS,jwt.(*middlewareService).Ctx-fm,jwt.(*middlewareService).SessionAuth-fm      
|---------|---------|---------|--------|----------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| GET    | /guser/jwtRefresh    | github.com/gogf/gf-jwt.(*GfJWTMiddleware).RefreshHandler-fm | guser.MiddlewareCORS,jwt.(*middlewareService).Ctx-fm,jwt.(*middlewareService).SessionAuth-fm      
|---------|---------|---------|--------|----------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| GET    | /guser/pingJwt       | gitee.com/qian/guser/api.GUserPing                             | guser.MiddlewareCORS,jwt.(*middlewareService).Ctx-fm,jwt.(*middlewareService).SessionAuth-fm      
|---------|---------|---------|--------|----------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| GET    | /guser/pingSession   | gitee.com/qian/guser/api.GUserPing                             | session.(*middlewareService).Ctx-fm,session.(*middlewareService).SessionAuth-fm                   
|---------|---------|---------|--------|----------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| GET    | /guser/pingToken     | gitee.com/qian/guser/api.GUserPing                             | guser.MiddlewareCORS,token.(*middlewareService).Ctx-fm,token.(*middlewareService).SessionAuth-fm  
|---------|---------|---------|--------|----------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| POST   | /guser/sessionLogin  | gitee.com/qian/guser/api.(*gUserSessionLogin).Login-fm         | session.(*middlewareService).Ctx-fm                                                               
|---------|---------|---------|--------|----------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| POST   | /guser/sessionLogout | gitee.com/qian/guser/api.(*gUserSessionLogin).Logout-fm        | session.(*middlewareService).Ctx-fm,session.(*middlewareService).SessionAuth-fm                   
|---------|---------|---------|--------|----------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| POST   | /guser/tokenLogin    | gitee.com/qian/guser/api.(*gUserTokenLogin).Login-fm           | guser.MiddlewareCORS,token.(*middlewareService).Ctx-fm                                            
|---------|---------|---------|--------|----------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| POST   | /guser/tokenLogout   | gitee.com/qian/guser/api.(*gUserTokenLogin).Logout-fm          | guser.MiddlewareCORS,token.(*middlewareService).Ctx-fm,token.(*middlewareService).SessionAuth-fm  
|---------|---------|---------|--------|----------------------|-------------------------------------------------------------|--------------------------------------------------------------------------------------------------|


**ps:**

如果是goLand编辑器开发,
可复制 https://gitee.com/qian/guser/blob/master/api.http 和 https://gitee.com/qian/guser/blob/master/http-client.env.json
到你项目任意位置,启动项目后,可以打开guser目录下的api.http文件,然后点击测试即可,(session测试需要到浏览器测试)
![图片](https://oss-qn.zacms.com/apihttp.jpg)



### 4. 业务代码中获取用户信息:

```
import (
    "gitee.com/qian/guser/shared"
)

func Hello(r *ghttp.Request) {
    user := shared.Context.Get(r.Context()).User
}
```

### 5. 项目中其他路由登录验证:
(demo是我测试项目的名称)   
(默认路由文件在router/router.go文件中,编辑路由文件)   

**以下中间件你按需引入即可,这里演示的3种验证**

```
import (
	middelwareToken "gitee.com/qian/guser/middleware/token"
    middelwareSession "gitee.com/qian/guser/middleware/session"
    middelwareJwt "gitee.com/qian/guser/middleware/jwt"
)

func init() {
	s := g.Server()
    //session验证的方式
	s.Group("/session", func(group *ghttp.RouterGroup) {
        //上下文
		group.Middleware(middelwareSession.GUserSessionMiddleware.Ctx)
        //路由验证
        group.Middleware(middelwareSession.GUserSessionMiddleware.SessionAuth)
        group.ALL("/authTest", demo.AuthTest)
	})
    //token验证的方式
	s.Group("/token", func(group *ghttp.RouterGroup) {
        //上下文
        group.Middleware(middelwareToken.GUserTokenMiddleware.Ctx)
        //路由验证
        group.Middleware(middelwareToken.GUserTokenMiddleware.SessionAuth)
        group.ALL("/authTest", demo.AuthTest)
	})
    //jwt验证的方式
	s.Group("/jwt", func(group *ghttp.RouterGroup) {
        //上下文
		group.Middleware(middelwareJwt.GUserJwtMiddleware.Ctx)
        //路由验证
        group.Middleware(middelwareJwt.GUserJwtMiddleware.SessionAuth)
        group.ALL("/authTest", demo.AuthTest)
	})
}
```

## 二次开发
- 可fork项目,然后根据自己业务修改
- 可下载源文件,放到项目中,然后替换引入的包路径即可