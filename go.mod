module gitee.com/qian/guser

go 1.14

require (
	github.com/gogf/gcache-adapter v0.1.1
	github.com/gogf/gf v1.15.4
	github.com/gogf/gf-jwt v1.1.2
)
