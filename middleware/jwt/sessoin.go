package middleware

import (
	"context"
	"gitee.com/qian/guser/model"
	"gitee.com/qian/guser/shared"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
)

var Session = sessionService{}

type sessionService struct {
}

const (
	// 用户信息存放在Session中的Key
	sessionKeyUser = "SessionKeyUser"
)

func (s *sessionService) GetUser(r *ghttp.Request) *model.Guser {
	var userInfo *model.Guser
	param := r.GetParam("JWT_PAYLOAD")
	gconv.Struct(param, &userInfo)
	if userInfo.Id > 0 {
		return userInfo
	}
	return nil
}

func (s *sessionService) RemoveUser(ctx context.Context) error {
	shareCtx := shared.Context.Get(ctx)
	if shareCtx != nil {
		return shareCtx.Session.Remove(sessionKeyUser)
	}
	return nil
}
