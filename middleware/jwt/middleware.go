package middleware

import (
	"gitee.com/qian/guser/library/response"
	"gitee.com/qian/guser/model"
	"gitee.com/qian/guser/shared"
	"github.com/gogf/gf-jwt/example/api"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

var GUserJwtMiddleware = middlewareService{}

type middlewareService struct {
}

// 自定义上下文对象
func (s *middlewareService) Ctx(r *ghttp.Request) {
	// 初始化，务必最开始执行
	customCtx := &model.Context{
		Data: make(g.Map),
	}
	shared.Context.Init(r, customCtx)
	// 将自定义的上下文对象传递到模板变量中使用
	r.Assigns(g.Map{
		"Context": customCtx,
	})
	// 执行下一步请求逻辑
	r.Middleware.Next()
}

func (s *middlewareService) SessionAuth(r *ghttp.Request) {
	api.Auth.MiddlewareFunc()(r)
	if userEntity := Session.GetUser(r); userEntity != nil {
		user := &model.ContextUser{
			Id:      userEntity.Id,
			Account: userEntity.Account,
		}
		shared.Context.SetUser(r.Context(), user)
	} else {
		response.JsonExit(r, 200, "请登录")
	}
	r.Middleware.Next()
}
