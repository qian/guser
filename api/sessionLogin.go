package api

import (
	"gitee.com/qian/guser/define"
	"gitee.com/qian/guser/library/response"
	"gitee.com/qian/guser/service"
	"github.com/gogf/gf/net/ghttp"
)

var GUserSessionLoginApi = gUserSessionLogin{}

type gUserSessionLogin struct {
}

func (s *gUserSessionLogin) Login(r *ghttp.Request) {
	var (
		data *define.GUserApiLoginReq
	)
	if err := r.Parse(&data); err != nil {
		response.JsonExit(r, 500, err.Error())
	}
	err := service.GUser.Login(r.Context(), data)
	if err != nil {
		response.JsonExit(r, 500, "登录失败")
	}
	response.Json(r, 200, "登录成功")
}

func (s *gUserSessionLogin) Logout(r *ghttp.Request) {
	err := service.GUser.LoginOut(r.Context())
	if err != nil {
		response.JsonExit(r, 500, "退出失败")
	}
	response.Json(r, 200, "退出成功")
}
