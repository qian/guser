package api

import (
	"gitee.com/qian/guser/library/response"
	"gitee.com/qian/guser/shared"
	"github.com/gogf/gf/net/ghttp"
)

func GUserPing(r *ghttp.Request) {
	id := shared.Context.Get(r.Context()).User
	response.Json(r, 200, "pong", id)
}
