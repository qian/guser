package api

import (
	"gitee.com/qian/guser/define"
	"gitee.com/qian/guser/library/response"
	"gitee.com/qian/guser/service"
	"gitee.com/qian/guser/shared"
	"github.com/gogf/gf/net/ghttp"
)

var GUserTokenLoginApi = gUserTokenLogin{}

type gUserTokenLogin struct {
}

func (s *gUserTokenLogin) Login(r *ghttp.Request) {
	var (
		data *define.GUserApiLoginReq
	)
	if err := r.Parse(&data); err != nil {
		response.JsonExit(r, 500, err.Error())
	}
	err := service.GUser.LoginToken(r.Context(), data)
	if err != nil {
		response.JsonExit(r, 500, "登录失败")
	}
	response.Json(r, 200, "登录成功", shared.Context.Get(r.Context()).Data)
}

func (s *gUserTokenLogin) Logout(r *ghttp.Request) {
	err := service.GUser.LogoutToken(r.Context())
	if err != nil {
		response.JsonExit(r, 500, "退出失败")
	}
	response.Json(r, 200, "退出成功")
}
