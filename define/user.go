package define

// Api用户登录
type GUserApiLoginReq struct {
	Passport string `v:"required#请输入账号"` // 账号
	Password string `v:"required#请输入密码"` // 密码(明文)
}

// Service用户登录
type GUserServiceLoginReq struct {
	Passport string `v:"required#请输入账号"` // 账号
	Password string `v:"required#请输入密码"` // 密码(明文)
}

// Middleware token
type GUserMiddlewareTokenReq struct {
	Token string `v:"required#验证失败"` // 账号
}
